# module-ecs-cluster/main.tf

resource "aws_ecs_cluster" "cluster" {
  for_each = var.clusters
  name     = each.value.cluster_name

  setting {
    name  = "containerInsights"
    value = each.value.container_insights_enabled
  }

  configuration {
    execute_command_configuration {
      kms_key_id = each.value.log_key_arn
      logging    = each.value.logging

      log_configuration {
        cloud_watch_encryption_enabled = each.value.cloud_watch_encryption_enabled
        cloud_watch_log_group_name     = each.value.cloud_watch_log_group_name
      }
    }
  }

  tags = {
    Environment = var.environment
    ManagedBy  = var.managed_by
  }
}

resource "aws_service_discovery_private_dns_namespace" "dns_namespace" {
  for_each    = var.dns_namespaces
  name        = each.value.name
  description = each.value.description
  vpc         = each.value.vpc_id

  tags = {
    Environment = var.environment
    ManagedBy  = var.managed_by
  }
}
