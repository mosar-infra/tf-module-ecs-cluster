# module-ecs outputs

output "cluster" {
  value = aws_ecs_cluster.cluster
}

output "dns_namespace" {
  value = aws_service_discovery_private_dns_namespace.dns_namespace
}
