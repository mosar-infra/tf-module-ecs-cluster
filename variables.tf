# module-ecs-cluster/variables

variable "environment" {}
variable "managed_by" {}
variable "clusters" {}
variable "dns_namespaces" {
  default = {}
}

